#include <stdio.h>
#include <stdlib.h>
//gcc heap.c -o heapbin

#define MAXARRAY 50000

int esq(int i){
    return (i*2);
}

int dir(int i){
    return (i*2+1);
}

int pai(int i){
    return (i/2);
}

void max_heapfy(int *vet, int n, int i){
	int e=esq(i), d=dir(i), maior=i;
	if (e<=n && vet[e] > vet[i]) maior=e;
	if (d<=n && vet[d] > vet[maior]) maior=d;
	if (maior != i){
		int temp=vet[i];
		vet[i] = vet[maior];
		vet[maior]=temp;
		max_heapfy(vet,n,maior);
	}
}

void build_max_heap(int *v,int n){
    int x=n,i,j=(n/2);
    for(i=j; i>=1; i--)
        max_heapfy(v,n,i);
}

void heapsort(int *v,int n){
    int i,j=n;
    build_max_heap(v,n);

    //DEBUG
    printf("APOS BUILD_MAX_HEAP:\n");
    for(int g=1; g<=n; g++) printf("%d ", v[g]);
    printf("\n");

    for(i=j;i>1;i--){
        int temp=v[i];
        v[i]=v[1];
        v[1]=temp;
        j--;
        max_heapfy(v,j,1);
        //j--;
    }
    /*for(i=j;i>1;i--){
        int temp=v[i];
        v[i]=v[1];
        v[1]=temp;
        j--;
        max_heapfy(v,j,1);
    } */
}

int main(void){
 int *vet, n = MAXARRAY;
 int i = 0;
 
 printf("Digite o tamanho do vetor de elementos a serem ordenados... ");
 scanf("%d",&n);
 if(n <= 0) return 0;
 vet = (int *) malloc(sizeof(int) * (n + 1));

 for(i = 1; i <= n; i++) vet[i] = rand() % n;
 
 printf("Antes de ordenar com heapsort...\n");
 for(i = 1; i <= n; i++) printf("%d ", vet[i]);
 printf("\n");

 heapsort(vet, n);

 printf("Depois de ordenar com heapsort...\n");
 for(i = 1; i <= n; i++) printf("%d ", vet[i]);
 printf("\n");

 free(vet);
 return 0;
}
