#include <stdio.h>
#include <stdlib.h>
//gcc heaptern.c -o teste

int k;

int filho(int indice, int pos) {
    return indice*k + pos;
}

int pai(int indice) {
    return (indice - (indice%k)) / k;
}

void min_heapfy(int *vet, int n, int i){
    //vetor de indices dos filhos
    int filhos[k], j, menor=i, temp;
    for(j = 1; j<=k; j++) {
        filhos[j-1] = filho(i, j);
    }

    for(j=0; j<k; j++) {
        if((filhos[j]<n) && (vet[filhos[j]] < vet[menor])) {
            menor = filhos[j];
        }
    }

	if (menor != i){
		temp=vet[i];
		vet[i] = vet[menor];
		vet[menor]=temp;
		min_heapfy(vet,n,menor);
	}

}

void build_min_heap(int *v, int n){
    int i, j=(n/k);

    for(i=j; i>=0; i--)
        min_heapfy(v,n,i);
}

void heapsort(int *v,int n){
    int i, j=n-1, temp;
    build_min_heap(v,n);

    //DEBUG
    printf("APOS BUILD_MIN_HEAP:\n");
    for(int g=0; g<n; g++) printf("%d ", v[g]);
    printf("\n");

    for(i=j; i>0; i--){
        temp=v[i];
        v[i]=v[0];
        v[0]=temp;
        min_heapfy(v, i, 0);
        //j--;
    }
    /*for(i=j; i>=0; i--){
        temp=v[i];
        v[i]=v[0];
        v[0]=temp;
        min_heapfy(v, i, 0);
    } */
}

int main(void){
 printf("Digite a ordem \"k\" da heap: ");
 scanf("%d", &k);

 int *vet, n;
 int i = 0;
 
 printf("Digite o tamanho do vetor de elementos a serem ordenados... ");
 scanf("%d",&n);
 if(n <= 0) return 0;
 vet = (int *) malloc(sizeof(int) * (n));

 for(i = 0; i < n; i++) vet[i] = rand() % n;
 
 printf("Antes de ordenar com heapsort...\n");
 for(i = 0; i < n; i++) printf("%d ", vet[i]);
 printf("\n");

 heapsort(vet, n);

 printf("Depois de ordenar com heapsort...\n");
 for(i = 0; i < n; i++) printf("%d ", vet[i]);
 printf("\n");

 free(vet);
 return 0;
}